from src.scripts.script_chats import ScriptChats
from src.utils import startup_config
from src.models.classes.bot import Bot
from pynput.keyboard import Key, Listener
import sys
import os
import time
import traceback
import threading


def write_error(e):
    print(traceback.format_exc())


def main():
    args = sys.argv
    if 'config' in args or not os.path.exists('settings.ini'):
        startup_config.configure()

    bot = Bot()
    bot.pause = False
    bot.router_mode = False
    # b.eyes.test_scan()
    # return

    for count in range(3):
        print(3 - count)
        time.sleep(1)
    print('[Press ESC to stop]')

    def on_press(key):
        if key == Key.esc:
            os.abort()
    Listener(on_press=on_press).start()

    def background():
        while True:
            time.sleep(1)
            if bot.pause:
                os.abort()

    def foreground():
        while True:
            try:
                ScriptChats(bot).start()

            except Exception as e:
                write_error(e)
                pass

    b = threading.Thread(name='background', target=background)
    f = threading.Thread(name='foreground', target=foreground)
    b.start()
    f.start()


if __name__ == "__main__":
    main()
