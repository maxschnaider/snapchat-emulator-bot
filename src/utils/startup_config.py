from src.models.classes.config import LocalConfig
from src.models import firebase
from pynput.keyboard import Key, Listener
import pyautogui
import os


def _setup_bot_id(config):
    print('Enter Bot ID:')

    def on_press(key):
        if key == Key.enter:
            config.bot_id = str(input())
            return False

        elif key == Key.esc:
            return False

    with Listener(on_press=on_press) as listener:
        listener.join()


def _setup_window_coords(config):
    window_position = []
    print('Move mouse to the upper left corner of Snapchat and press Enter')

    def on_press(key):
        if key == Key.enter:
            if 0 <= len(window_position) <= 1:
                x, y = pyautogui.position()
                # if subprocess.call("system_profiler SPDisplaysDataType | grep 'Retina'", shell=True) == 0:
                #     x = x*2
                #     y = y*2
                #     config.retina_display = True
                # else:
                #     config.retina_display = False
                window_position.append([x, y])
                if len(window_position) == 1:
                    print('Move mouse to the bottom right corner of Snapchat and press Enter')
                else:
                    return False

        elif key == Key.esc:
            return False

    with Listener(on_press=on_press) as listener:
        listener.join()

    config.window_position = window_position


def configure():
    config = LocalConfig()
    _setup_bot_id(config)
    # _setup_window_coords(config)

