import pyautogui
import pyperclip
import time
from datetime import datetime


class Hands:
    def __init__(self, bot):
        self.bot = bot
        self.eyes = bot.eyes
        self.window = bot.window
        self.mouse_events_count = 0

    def move_to(self, coords):
        pyautogui.moveTo(coords[0], coords[1])
        self.mouse_events_count += 1
        if self.mouse_events_count > 50:
            self.app_stuck_checkout()

    def click(self, filename=None, sleep=0.05, accuracy=None, coords=None, monitor=None, screenshot=False):
        if filename:
            coords = self.eyes.find(filename=filename, accuracy=accuracy, monitor=monitor)

        if not coords:
            self.bot.log('Cannot find [' + (filename if filename else 'None') + ']')
            return False

        self.move_to(coords)
        pyautogui.click()

        if screenshot:
            self.bot.capture_screen()
            sleep = sleep-2 if sleep > 2 else 0.01

        time.sleep(sleep)

        self.bot.log('Click [' + (filename if filename else str(coords)) + ']')
        return coords

    def long_press(self, filename=None, sleep=1, accuracy=None, coords=None, screenshot=False):
        if filename:
            coords = self.eyes.find(filename=filename, accuracy=accuracy)

        if not coords:
            self.bot.log('Cannot find [' + (filename if filename else 'None') + ']')
            return False

        self.move_to(coords)
        time.sleep(0.1)
        pyautogui.mouseDown()
        time.sleep(1)
        pyautogui.mouseUp()

        if screenshot:
            self.bot.capture_screen()
            sleep = sleep-2 if sleep > 2 else 0.01

        time.sleep(sleep)

        self.bot.log('Long Press [' + (filename if filename else str(coords)) + ']')
        return coords

    def scroll(self, direction):
        print('scroll '+direction)
        if direction == 'up':
            self.move_to([self.window.center_x, self.window.center_y])
            pyautogui.mouseDown()
            pyautogui.move(0, self.window.center_x - self.window.top, 0.5)
        elif direction == 'down':
            self.move_to([self.window.center_x, self.window.bottom - self.window.height * 0.4])
            pyautogui.mouseDown()
            pyautogui.move(0, -(self.window.height * 0.5 - self.window.top), 0.5)
        pyautogui.mouseUp()
        time.sleep(1)

    def swipe(self, direction):
        print('swipe '+direction)
        if direction == 'right':
            self.move_to([self.window.left + 30, self.window.center_y])
            pyautogui.mouseDown()
            pyautogui.move(self.window.width - 50, 0, 0.5)
        elif direction == 'left':
            self.move_to([self.window.right - 30, self.window.center_y])
            pyautogui.mouseDown()
            pyautogui.move(-self.window.width - 50, 0, 0.5)
        pyautogui.mouseUp()
        time.sleep(1)

    def copy(self, text):
        pyperclip.copy(text)

    def paste(self):
        pyperclip.paste()

    def back(self, sleep=1):
        self.bot.log('back')
        self.click(coords=[self.window.center_x-60, self.window.bottom+25], sleep=sleep)

    def refresh_chats(self):
        self.open_chats_screen()
        self.open_camera_screen()
        self.open_chats_screen()

    def open_chats_screen(self):
        i = 0
        while not self.eyes.see_chat_screen():
            if i > 4:
                self.app_stuck_checkout()
            if self.eyes.see_camera_screen():
                self.swipe('right')
            else:
                self.back()
            i += 1
        time.sleep(1)

    def open_camera_screen(self):
        i = 0
        while not (self.eyes.find('camera_screen.png') or self.eyes.find('camera_screen1.png')):
            if i > 4:
                self.app_stuck_checkout()
            self.back()
            i += 1
        return True

    def open_profile_screen(self):
        if self.bot.eyes.see_profile_screen():
            return True

        self.bot.hands.open_chats_screen()
        for _ in range(50):
            time.sleep(0.2)
            if self.bot.eyes.find('search.png') or self.bot.eyes.find('search1.png'):
                self.bot.hands.click(coords=[self.bot.window.left + 20, self.bot.window.top + 20], sleep=3)
                return self.bot.eyes.see_profile_screen()

    def click_bottom_input(self):
        self.click(coords=[self.bot.window.center_x, self.bot.window.bottom-30], sleep=2)

    def app_stuck_checkout(self):
        self.mouse_events_count = 0

        if self.click('access_deny_btn.png', 2):
            raise ValueError('Restart scripts')

        wide_monitor = self.window.subwindow(left=1, right=1900).monitor
        (self.click('close_crashed_app.png', monitor=wide_monitor, sleep=10) or
         self.click('close_crashed_app_small.png', 10))

        self.click('win_update_btn.png', monitor=wide_monitor, sleep=2)

        if self.click('reopen_snapchat.png', monitor=wide_monitor, sleep=5) or \
                self.click('snapchat_app_icon.png', monitor=wide_monitor, sleep=5):
            raise ValueError('Restart scripts')

    def hide_keyboard(self):
        input_coords = self.bot.eyes.find('input.png')
        if self.bot.eyes.find('keyboard1.png') or self.bot.eyes.find('keyboard2.png') or\
                (input_coords and input_coords[1] < self.bot.window.bottom - 100):
            self.bot.hands.back()

    def show_keyboard(self) -> bool:
        input_coords = self.bot.eyes.find('input.png')
        if self.bot.eyes.find('keyboard1.png') or self.bot.eyes.find('keyboard2.png') or\
                (input_coords and input_coords[1] < self.bot.window.bottom - 100):
            return True

        input_with_text_coords = self.bot.eyes.find('input_with_text.png')
        if (input_coords and input_coords[1] > self.bot.window.bottom - 100) or \
            (input_with_text_coords and input_with_text_coords[1] > self.bot.window.bottom - 100):
            self.bot.hands.click_bottom_input()
            return True

    def hide_snapchat(self):
        i = 0
        while not self.eyes.find('snapchat_app_icon.png', monitor=self.window.subwindow(left=1, right=1900).monitor) and i < 5:
            self.back()
            i += 1
        return True
