import requests


class Tg:
    def __init__(self):
        self.url = 'https://api.telegram.org/bot1087563755:AAHPdUoSS2Ic7jfIQKTJYjIjbyIivVX58DA/'
        self.chat_id = '-1001218059425'

    def send_message(self, text, disable_notification=False):
        method = 'sendMessage'
        r = requests.post(
            self.url+method,
            data={
                'chat_id': self.chat_id,
                'text': text,
                'disable_web_page_preview': True,
                'disable_notification': disable_notification
            }
        )
