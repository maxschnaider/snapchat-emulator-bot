import cv2
import numpy
import time
import mss
import pytesseract
import os
import re
import imutils


class Eyes:
    def __init__(self, bot):
        self.bot = bot
        self.accuracy = bot.config.eyes_accuracy
        self.window = bot.window

    def find(self, filename, accuracy=None, last=False, all=False, monitor=None):
        monitor = monitor if monitor else self.window.monitor.copy()
        accuracy = accuracy if accuracy else self.accuracy

        small_image = cv2.imread('./src/assets/'+filename)
        small_image = cv2.cvtColor(small_image, cv2.COLOR_RGB2RGBA)
        w, h = small_image.shape[:-1]
        large_image = self.capture_screen(monitor)
        result = cv2.matchTemplate(small_image, large_image, cv2.TM_CCOEFF_NORMED)
        loc = numpy.where(result >= accuracy)
        coords = []
        for pt in zip(*loc[::-1]):
            # cv2.rectangle(large_image, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 1)
            center_x = int(pt[0] + (h / 2))
            center_y = int(pt[1] + (w / 2))
            center_coords = [monitor.left + center_x, monitor.top + center_y]
            if not (last or all):
                print('Found [' + filename + ']')
                return center_coords
            coords.append(center_coords)
        if len(coords) > 0 and (last or all):
            return coords[-1] if last else coords
        print('Not found [' + filename + ']')
        return False

    def capture_screen(self, monitor=None, color=cv2.COLOR_RGB2RGBA, threshold=None):
        screenshot = mss.mss().grab((monitor if monitor else self.window.monitor).to_dict())
        screenshot = cv2.cvtColor(numpy.array(screenshot), color)
        if threshold:
            screenshot = cv2.threshold(screenshot, threshold, 255, cv2.THRESH_BINARY)[1]
        return numpy.array(screenshot)

    def capture_screen_png(self, monitor=None):
        screenshot = mss.mss().grab((monitor if monitor else self.window.monitor).to_dict())
        return mss.tools.to_png(screenshot.rgb, screenshot.size)

    def read_text(self, monitor=None, threshold=170) -> str:
        pytesseract.pytesseract.tesseract_cmd = os.path.dirname(__file__)[0:-len('/utils')] + r'\\libs\\Tesseract\\tesseract'
        image = self.capture_screen(
            monitor=(monitor if monitor else self.window.monitor),
            color=cv2.COLOR_BGR2GRAY,
            threshold=threshold)
        recognized_string = pytesseract.image_to_string(image)
        return recognized_string.strip()

    def see_chat_screen(self) -> bool:
        return (self.find('chats_screen1.png')\
                or self.find('chats_screen2.png')\
                or self.find('chats_screen3.png'))\
                    and self.find('white_bg.png')

    def see_camera_screen(self) -> bool:
        return self.find('camera_screen.png') or self.find('camera_screen1.png')

    def see_profile_screen(self) -> bool:
        return self.find('profile_screen_proof1.png') or self.find('friends_plus.png')

    def test_scan(self):
        scanner_height = 40
        steps_per_scanner_height = 3
        scanner_step_height = scanner_height / steps_per_scanner_height
        step = 0
        empty_steps_max_chain_len = 0
        empty_steps_chain_len = 0
        top = 0

        pytesseract.pytesseract.tesseract_cmd = '/usr/local/Cellar/tesseract/4.1.1/bin/tesseract'
        img = cv2.imread('./src/assets/1.png')
        w, h = img.shape[:-1]

        while top + scanner_height < h:
            crop_img_to_scan = img[int(top):int(top+scanner_height), int(0):int(w)]
            if len(crop_img_to_scan) == 0:
                break
            crop_img_to_scan = cv2.cvtColor(crop_img_to_scan, cv2.COLOR_BGR2GRAY)
            crop_img_to_scan = cv2.threshold(crop_img_to_scan, 60, 255, cv2.THRESH_BINARY)[1]
            recognized_string = pytesseract.image_to_string(crop_img_to_scan)

            self.bot.log(step)
            self.bot.log(recognized_string)

            if recognized_string:
                empty_steps_chain_len = 0
            else:
                empty_steps_chain_len += 1
                if empty_steps_chain_len > empty_steps_max_chain_len:
                    empty_steps_max_chain_len = empty_steps_chain_len
            top = top + scanner_step_height
            step += 1

        empty_lines_max_chain_len = float(empty_steps_max_chain_len / steps_per_scanner_height)
        is_photo = empty_lines_max_chain_len > 2
        self.bot.log(empty_lines_max_chain_len)
        self.bot.log(is_photo)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.threshold(img, 60, 255, cv2.THRESH_BINARY)[1]
        cv2.imwrite('./src/assets/2.png', img)
        self.bot.log(pytesseract.image_to_string(img))
