

class Monitor:
    def __init__(self, window):
        self.top = window.top
        self.left = window.left
        self.width = window.width
        self.height = window.height

    def copy(self):
        return Monitor(self)

    def to_dict(self):
        return {
            'top': int(self.top),
            'left': int(self.left),
            'width': int(self.width),
            'height': int(self.height)
        }


class Window:
    def __init__(self, bot):
        self.bot = bot
        self.window_position = self.bot.config.window_position
        self.username_position = self.bot.config.username_position
        self.left = int(self.window_position[0][0])
        self.top = int(self.window_position[0][1])
        self.right = int(self.window_position[1][0])
        self.bottom = int(self.window_position[1][1])
        self.width = int(self.right - self.left)
        self.height = int(self.bottom - self.top)
        self.center_x = int(self.left + self.width/2)
        self.center_y = int(self.top + self.height/2)
        self.__monitor = Monitor(self)

    @property
    def monitor(self):
        return self.__monitor.copy()

    @monitor.setter
    def monitor(self, monitor):
        self.__monitor = monitor

    def subwindow(self, left=None, top=None, right=None, bottom=None):
        subwindow = Window(bot=self.bot)
        subwindow.top = int(top) if top else self.top
        subwindow.left = int(left) if left else self.left
        subwindow.right = int(right) if right else self.right
        subwindow.bottom = int(bottom) if bottom else self.bottom
        subwindow.width = int(subwindow.right - subwindow.left)
        subwindow.height = int(subwindow.bottom - subwindow.top)
        subwindow.center_x = int(subwindow.left + subwindow.width/2)
        subwindow.center_y = int(subwindow.top + subwindow.height/2)
        subwindow.monitor = Monitor(subwindow)
        return subwindow

    def y_for_height_percent(self, percent):
        return self.top + self.height*percent

    def x_for_width_percent(self, percent):
        return self.left + self.width*percent
