from datetime import datetime
import time
import re
import random


class Message:
    def __init__(self, chat, message_id, is_incoming, is_step, text, is_photo, step=None, timestamp=None):
        self.chat = chat
        self.id = str(message_id)
        self.is_incoming = is_incoming
        self.is_step = is_step
        self.text = text
        self.is_photo = is_photo
        self.step = step if step else chat.step
        self.timestamp = timestamp if timestamp else datetime.utcnow().isoformat()

    def to_dict(self):
        return {
            'id': self.id,
            'chat_id': self.chat.id,
            'is_incoming': self.is_incoming,
            'is_step': self.is_step,
            'text': self.text,
            'is_photo': self.is_photo,
            'step': self.step,
            'timestamp': self.timestamp
        }

    def log(self):
        self.id = datetime.utcnow().isoformat().replace(':', '').replace('.', '')
        self.chat.bot.db.log_message(self)


class Chat:
    def __init__(self, bot, username=None):
        self.bot = bot
        self.id = username
        self.fetch()

    # Properties
    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def step(self):
        return self.__step

    @step.setter
    def step(self, step):
        self.__step = step

    @property
    def messages(self):
        return self.__messages

    @messages.setter
    def messages(self, messages):
        self.__messages = messages

    @property
    def last_message(self) -> Message:
        return self.messages[-1] if len(self.messages) > 0 else None

    # Methods
    def fetch(self):
        chat_data = self.bot.db.get_chat(self.bot.account.username, self.id) if self.id else {}
        self.step = chat_data['step'] if 'step' in chat_data else 0
        self.bot.log('Chat step: ' + str(self.step))
        self.messages = []
        if 'messages' in chat_data:
            for message_data in chat_data['messages']:
                msg = Message(chat=self,
                              message_id=message_data['id'] if 'id' in message_data else 0,
                              is_incoming=message_data['is_incoming'] if 'is_incoming' in message_data else None,
                              is_step=message_data['is_step'] if 'is_step' in message_data else None,
                              text=message_data['text'] if 'text' in message_data else None,
                              is_photo=message_data['is_photo'] if 'is_photo' in message_data else None,
                              step=message_data['step'] if 'step' in message_data else None,
                              timestamp=message_data['timestamp'] if 'timestamp' in message_data else None)
                self.messages.append(msg)

    def to_dict(self):
        chat_dict = {
            'step': self.step
        }
        if len(self.messages) > 0:
            messages = []
            for i in range(0, len(self.messages)):
                messages.append(self.messages[i].to_dict())
            chat_dict['messages'] = messages

        return chat_dict

    def get_message_unique_id(self) -> str:
        # return str(int(list(self.messages.keys())[len(self.messages)-1])+1) if len(self.messages) > 0 else 0
        return str(len(self.messages))

    def add_message(self, message):
        self.bot.log('add_message')
        if message:
            self.messages.append(message)
            self.step = self.step + 1 if message.is_step and not message.is_incoming else self.step
            self.bot.db.update_chat(self.bot.account.username, chat=self)
            message.log()
            if not message.is_incoming:
                self.bot.message = message.text

    # UI/UX LOGIC
    def open_chat(self, coords, say_hi=False):
        if self.open_chat_info(coords):
            self.bot.config.fetch()
            username = self.get_username()
            if 'snapp' not in username:
                if not say_hi:
                    self.bot.hands.click_bottom_input()
                    return
                else:
                    self.set_username()
                    username = self.get_username()
                    if 'snapp' not in username:
                        return

            self.id = username
            self.fetch()

            if say_hi and self.step > 1:
                self.step = 0

            self.bot.hands.click_bottom_input()
            return True

    def open_chat_info(self, chat_coords):
        self.bot.log('open_chat_info')
        self.bot.hands.long_press(coords=chat_coords, sleep=2)

        # check if group
        if self.bot.eyes.find('group_menu.png'):
            self.bot.hands.click(coords=[self.bot.window.center_x, self.bot.window.bottom - 80], sleep=1)
            self.bot.hands.click('leave_group.png', 1)
            self.bot.hands.click('leave_group_btn.png', 2)

        elif self.bot.eyes.find('chat_menu.png'):
            if self.bot.eyes.find('chat_menu_full.png'):
                self.bot.hands.click(coords=[self.bot.window.center_x, self.bot.window.y_for_height_percent(0.55)], sleep=2.5)
                if not self.is_black_profile_to_report():
                    return True
            else:
                for _ in range(2):
                    self.bot.hands.click(coords=chat_coords, sleep=0.2)

    def is_black_profile_to_report(self):
        if self.bot.hands.click('black_profile_menu_button.png'):
            self.bot.hands.click('report_button.png')
            self.bot.hands.click(coords=[self.bot.window.center_x, self.bot.window.center_y], sleep=1) # spam
            self.bot.hands.click(coords=[self.bot.window.center_x, self.bot.window.top + 200], sleep=1) # added me on snapchat
            self.bot.hands.click(coords=[self.bot.window.center_x, self.bot.window.bottom - 150], sleep=1) # report
            self.bot.hands.click(coords=[self.bot.window.center_x, self.bot.window.center_y]) # block friend
            return True


    # def set_delete_messages_daily(self):
    #     self.bot.hands.click(coords=[self.bot.window.top+20, self.bot.window.right-20], sleep=2)
    #     self.bot.hands.click('chat_menu_delete_messages.png', 2)
    #     self.bot.hands.click('delete_messages_daily_btn.png', 2)
    #     self.bot.hands.back()

    def get_unique_username(self) -> str:
        self.bot.log('get_unique_username')
        new_chat_id = self.bot.account.chats_count
        while True:
            new_chat_id += 1
            new_username = 'snappy'+str(new_chat_id)
            new_chat = Chat(bot=self.bot, username=new_username)
            if new_chat.step == 0:
                self.bot.account.chats_count = new_chat_id
                return new_username

    def set_username(self):
        self.bot.log('set_username')
        if not self.bot.eyes.find('chat_info.png'):
            ValueError('Chat info screen not found')

        def find_input():
            return self.bot.eyes.find(
                'input_username.png',
                monitor=self.bot.window.subwindow(
                    top=self.bot.window.top + self.bot.window.height * 0.3,
                    bottom=self.bot.window.bottom - self.bot.window.height * 0.3,
                    left=self.bot.window.left + self.bot.window.width * 0.5,
                    right=self.bot.window.right - self.bot.window.width * 0.2).monitor)

        self.bot.hands.click(coords=[self.bot.window.center_x, self.bot.window.username_position[0]], sleep=1)
        input_coords = find_input()
        if not input_coords:
            self.bot.hands.click(coords=[self.bot.window.center_x, self.bot.window.username_position[1]], sleep=1)
            input_coords = find_input()

        self.bot.hands.copy(self.get_unique_username())
        self.bot.hands.long_press(coords=input_coords)
        if not self.bot.eyes.find('select_all.png'):
            self.bot.hands.click('long_press_more.png', 1)
        self.bot.hands.click('select_all.png', 2)
        self.bot.hands.click('paste.png', 0.5)
        self.bot.hands.click('save_username.png', 5)

    def get_username(self):
        self.bot.log('get_username')
        if not self.bot.eyes.find('chat_info.png'):
            ValueError('Chat info screen not found')

        raw_username = self.bot.eyes.read_text(threshold=150).replace('.', '')
        self.bot.log('Raw username: ' + raw_username.split()[0])
        username = raw_username
        if 'nappy' in raw_username:
            user_id = raw_username.split()[0].lower() \
                .replace('sSnappy', '') \
                .replace('snappy', '') \
                .replace('s', '8') \
                .replace('?', '9') \
                .replace('o', '0') \
                .replace('&', '8')
            username = 'snappy'
            for char in user_id:
                number = re.search(r'[0-9]', char)
                if number:
                    username += number[0]
            self.bot.log('Username: ' + username)

        return username

    def read_incoming_message(self) -> bool:
        self.bot.log('read_incoming_message')
        self.bot.hands.hide_keyboard()
        my_last_message_coords = self.bot.eyes.find(
            'my_message_end.png',
            accuracy=0.85,
            last=True,
            monitor=self.bot.window.subwindow(bottom=self.bot.window.bottom-80, right=self.bot.window.left+15).monitor)
        my_last_message_bottom = my_last_message_coords[1]+30 if my_last_message_coords \
            else self.bot.window.top + self.bot.window.height * 0.075

        new_message_coords = self.bot.eyes.find(
            'incoming_message_end.png',
            accuracy=0.85,
            last=True,
            monitor=self.bot.window.subwindow(bottom=self.bot.window.bottom-80, right=self.bot.window.left+15).monitor)
        if not new_message_coords:
            return False
        new_message_bottom = new_message_coords[1]

        monitor = self.bot.window.subwindow(
            top=my_last_message_bottom,
            bottom=new_message_bottom,
            left=self.bot.window.left+15
        ).monitor
        self.bot.log(monitor.to_dict())
        if monitor.height <= 0:
            return False

        text, is_photo = self.scan(monitor)
        self.bot.log('TEXT: '+text)
        self.bot.log(is_photo)

        if len(text) > 0:
            self.add_message(Message(chat=self,
                                     message_id=self.get_message_unique_id(),
                                     is_incoming=True,
                                     is_step=False,
                                     text=text,
                                     is_photo=False))
            self.reply_on_keywords(text)

        if is_photo:
            self.add_message(Message(chat=self,
                                     message_id=self.get_message_unique_id(),
                                     is_incoming=True,
                                     is_step=False,
                                     text='',
                                     is_photo=is_photo))
        return True

    def reply(self) -> bool:
        if self.bot.config.messages_hourly_limit < self.bot.hourly_messages_count:
            return

        msg = ''
        is_step = False

        if self.step == 0:
            msg = self.bot.config.hi_message
            is_step = True

        elif self.step == 1:
            msg = self.bot.config.step1_message
            is_step = True

        elif self.step == 2:
            if self.last_message.is_photo:
                msg = self.bot.config.step2_message
                is_step = True
            elif not self.is_wait_proof_message_sent():
                msg = self.bot.config.step1_wait_proof_message

        # elif self.step == 3:
        #     if self.last_message.is_photo:
        #         msg = self.bot.config.step3_message
        #         is_step = True
        #     elif not self.is_wait_proof_message_sent():
        #         msg = self.bot.config.step2_wait_proof_message
        #
        # elif self.step == 4:
        #     if self.last_message.is_photo:
        #         msg = self.bot.config.step4_message
        #     elif not self.is_wait_proof_message_sent():
        #         msg = self.bot.config.step3_wait_proof_message

        msg = self.randomized_message(msg)

        if self.send_message(msg):
            time.sleep(5)
            self.save_message_in_chat()

            self.add_message(Message(chat=self,
                                     message_id=self.get_message_unique_id(),
                                     is_incoming=False,
                                     is_step=is_step,
                                     text=msg,
                                     is_photo=False,
                                     step=self.step))
            return True

    def reply_on_keywords(self, incoming_message):
        msg = ''
        if 'scam' in incoming_message:
            msg = "it's not scam, just regular giveaway, check proofs in my stories"
        elif ' bet ' in incoming_message:
            msg = 'bet'
        elif 'fake' in incoming_message:
            msg = "it's not fake, I usually make this giveaway and post proofs"
        # elif 'real' in incoming_message:
        #     msg = "it's real, check my stories for proofs"
        elif 'pay' in incoming_message:
            msg = "I'll pay you in any convenient way after you finish steps, don't worry"
        elif ' bot ' in incoming_message or ' bot?' in incoming_message:
            msg = "what do you mean bot lmao"

        self.send_message(msg)

    def send_message(self, msg) -> bool:
        if self.bot.eyes.find('input_with_text.png'):
            self.bot.hands.show_keyboard()
            (self.bot.hands.click('enter1.png') or
             self.bot.hands.click('enter2.png') or
             self.bot.hands.click('enter3.png') or
             self.bot.hands.click('enter4.png'))

        self.bot.log('REPLY: '+msg)
        if len(msg) > 0:
            self.bot.hands.copy(msg)
            if not self.bot.hands.show_keyboard():
                return

            if not self.bot.hands.long_press('input.png'):
                input_coords = [self.bot.window.center_x, 620]
                self.bot.hands.long_press(coords=input_coords)
                if not self.bot.eyes.find('select_all.png'):
                    self.bot.hands.click('long_press_more.png', 0.5)
                self.bot.hands.click('select_all.png', 1)

            # double check
            if not self.bot.eyes.find('paste.png'):
                if not self.bot.hands.show_keyboard():
                    return
                self.bot.hands.long_press('input.png')

            if self.bot.hands.click('paste.png', 0.2) and \
                    (self.bot.hands.click('enter1.png')
                     or self.bot.hands.click('enter2.png')
                     or self.bot.hands.click('enter3.png')
                     or self.bot.hands.click('enter4.png')):
                return True

    def save_message_in_chat(self):
        new_message_coords = self.bot.eyes.find(
            'my_message.png',
            accuracy=0.85,
            last=True,
            monitor=self.bot.window.subwindow(
                bottom=self.bot.window.bottom - 80,
                right=self.bot.window.left + 20).monitor)
        if new_message_coords:
            self.bot.hands.long_press(coords=[new_message_coords[0] + 5, new_message_coords[1]])  # save
            self.bot.hands.click('save_message.png')

    def wait_message(self) -> str:
        self.bot.log('Waiting for message...')
        self.bot.capture_screen()
        self.bot.message = ''
        while not self.bot.message:
            time.sleep(1)
            self.bot.fetch()
        return self.bot.message

    def scan(self, monitor=None) -> (str, bool):
        self.bot.log('scan')
        if self.step <= 1:
            text = self.bot.eyes.read_text(monitor=monitor)
            return text, False

        scanner_height = 35
        steps_per_scanner_height = 2
        scanner_step_height = scanner_height / steps_per_scanner_height
        step = 0
        empty_steps_max_chain_len = 0
        empty_steps_chain_len = 0

        is_photo = False

        monitor = monitor if monitor else self.bot.window.monitor
        scanner_monitor = monitor.copy()
        scanner_monitor.height = scanner_height

        while scanner_monitor.top + scanner_monitor.height < monitor.top + monitor.height and not is_photo:
            self.bot.log('Step: '+str(step))
            scanned_string = self.bot.eyes.read_text(scanner_monitor, threshold=60)
            text = ''
            for char in scanned_string:
                letter = re.search(r'[a-zA-Z0-9]', char)
                if letter:
                    text += letter[0]
            if len(text) > 0:
                self.bot.log(text)
                empty_steps_chain_len = 0
            else:
                empty_steps_chain_len += 1
                if empty_steps_chain_len > empty_steps_max_chain_len:
                    empty_steps_max_chain_len = empty_steps_chain_len
                    empty_lines_max_chain_len = float(empty_steps_max_chain_len / steps_per_scanner_height)
                    is_photo = empty_lines_max_chain_len >= 2
            scanner_monitor.top = scanner_monitor.top + scanner_step_height
            step += 1

        text = self.bot.eyes.read_text(monitor)
        text = text.lower().replace(self.id, '').replace('snappy', '').replace('\n', ' ').strip()

        return text, is_photo

    def is_wait_proof_message_sent(self):
        message_was_sent = False
        for message in self.messages[::-1]:
            if message.is_step:
                break
            if not message.is_incoming:
                message_was_sent = True
                break
        return message_was_sent

    def randomized_message(self, msg):
        phrases = []
        for phrase in msg.split():
            if '|' in phrase:
                phrase_variants = phrase.split('|')
                random_phrase = phrase_variants[random.randint(0, len(phrase_variants)-1)].replace('_', ' ')
                phrases.append(random_phrase)
            else:
                phrases.append(phrase)

        return ' '.join(phrases)
