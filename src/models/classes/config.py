from configobj import ConfigObj
import json


class LocalConfig:
    def __init__(self):
        self.config = ConfigObj('settings.ini', list_values=False, encoding='utf-8')
        self.app_name = str(self.config['Firebase']['app_name'])

    # Properties
    @property
    def window_position(self):
        return json.loads(str(self.config['Snapchat']['window_position']))

    @window_position.setter
    def window_position(self, window_position):
        self.config['Snapchat']['window_position'] = json.dumps(window_position)
        self.config.write()

    @property
    def bot_id(self):
        return str(self.config['Firebase']['bot_id']) if 'bot_id' in self.config['Firebase'] else None

    @bot_id.setter
    def bot_id(self, bot_id):
        self.config['Firebase']['bot_id'] = bot_id
        self.config.write()


class Config:
    def __init__(self, bot):
        local_config = LocalConfig()
        self.app_name = local_config.app_name
        self.bot_id = local_config.bot_id
        self.bot = bot
        self.bot.id = self.bot_id
        self.fetch()

    def to_dict(self):
        return {
            'window_position': self.window_position,
            'username_position': self.username_position,
            'eyes_accuracy': self.eyes_accuracy,
            'add_friends_max_count_per_cycle': self.add_friends_max_count_per_cycle,
            'hi_message': self.hi_message,
            'step1_message': self.step1_message_original,
            'step1_wait_proof_message': self.step1_wait_proof_message,
            'step2_message': self.step2_message,
            'step2_wait_proof_message': self.step2_wait_proof_message,
            'step3_message': self.step3_message,
            'step3_wait_proof_message': self.step3_wait_proof_message,
            'step4_message': self.step4_message,
            'friends_hourly_limit': self.friends_hourly_limit,
            'messages_hourly_limit': self.messages_hourly_limit
        }

    def fetch(self):
        db_config = self.bot.db.get_config()
        db_bot_config = self.bot.db.get_bot_config()
        db_bot_config = db_bot_config if db_bot_config else db_config

        self.window_position = db_bot_config['window_position'] if 'window_position' in db_bot_config else [[669, 51], [1247, 987]]
        self.username_position = db_config['username_position']
        self.eyes_accuracy = db_bot_config['eyes_accuracy'] if 'eyes_accuracy' in db_bot_config else db_config['eyes_accuracy']
        self.add_friends_max_count_per_cycle = db_config['add_friends_max_count_per_cycle']

        self.hi_message = db_config['hi_message']
        self.step1_message_original = db_config['step1_message']
        self.step1_message = self.step1_message_original + ' ' + self.bot.account.step1_video_url
        self.step1_wait_proof_message = db_config['step1_wait_proof_message']
        self.step2_message = db_config['step2_message']
        self.step2_wait_proof_message = db_config['step2_wait_proof_message']
        self.step3_message = db_config['step3_message']
        self.step3_wait_proof_message = db_config['step3_wait_proof_message']
        self.step4_message = db_config['step4_message']

        self.friends_hourly_limit = db_bot_config['friends_hourly_limit'] if 'friends_hourly_limit' in db_bot_config else db_config['friends_hourly_limit'] if 'friends_hourly_limit' in db_config else 0
        self.messages_hourly_limit = db_bot_config['messages_hourly_limit'] if 'messages_hourly_limit' in db_bot_config else db_config['messages_hourly_limit'] if 'messages_hourly_limit' in db_config else 0
