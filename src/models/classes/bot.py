from src.models import firebase
from src.utils import eyes, hands, tg
from src.models.classes import config, window, chat, account
import time
from datetime import datetime


class Bot:
    def __init__(self):
        self.db = firebase.Db()
        self.fetch()
        self.account = account.Account(self)
        self.config = config.Config(self)
        self.window = window.Window(self)
        self.eyes = eyes.Eyes(self)
        self.hands = hands.Hands(self)
        self.chat = chat.Chat(self)
        self.tg = tg.Tg()

    # Properties
    @property
    def status(self):
        return self.__status

    @status.setter
    def status(self, status):
        self.__status = status
        self.db.update_bot(self)

    @property
    def pause(self):
        return self.__pause

    @pause.setter
    def pause(self, pause):
        self.__pause = pause
        self.db.set_bot_key_value('pause', pause)

    @property
    def message(self):
        return None if self.__message == '' else self.__message

    @message.setter
    def message(self, message):
        self.__message = message
        self.last_message_timestamp = datetime.utcnow().isoformat()
        self.db.update_bot(self)

    @property
    def last_screenshot_url(self):
        return self.__last_screenshot_url

    @last_screenshot_url.setter
    def last_screenshot_url(self, last_screenshot_url):
        self.__last_screenshot_url = last_screenshot_url
        self.db.update_bot(self)

    @property
    def last_story_timestamp(self):
        return self.__last_story_timestamp

    @last_story_timestamp.setter
    def last_story_timestamp(self, last_story_timestamp):
        self.__last_story_timestamp = last_story_timestamp
        self.db.update_bot(self)

    @property
    def last_traffic_counting_timestamp(self):
        return self.__last_traffic_counting_timestamp

    @last_traffic_counting_timestamp.setter
    def last_traffic_counting_timestamp(self, last_traffic_counting_timestamp):
        self.__last_traffic_counting_timestamp = last_traffic_counting_timestamp
        self.db.update_bot(self)

    @property
    def traffic(self):
        return self.__traffic

    @traffic.setter
    def traffic(self, traffic):
        self.__traffic = traffic
        self.db.update_bot(self)

    @property
    def router_volume(self):
        return self.__router_volume

    @router_volume.setter
    def router_volume(self, router_volume):
        self.__router_volume = router_volume
        self.db.set_bot_key_value('router_volume', router_volume)

    @property
    def router_mode(self):
        return self.__router_mode

    @router_mode.setter
    def router_mode(self, router_mode):
        self.__router_mode = router_mode
        self.db.set_bot_key_value('router_mode', router_mode)

    @property
    def last_router_mode_run_timestamp(self):
        return self.__last_router_mode_run_timestamp

    @last_router_mode_run_timestamp.setter
    def last_router_mode_run_timestamp(self, last_router_mode_run_timestamp):
        self.__last_router_mode_run_timestamp = last_router_mode_run_timestamp
        self.db.set_bot_key_value('last_router_mode_run_timestamp', last_router_mode_run_timestamp)

    @property
    def hourly_new_friends_count(self):
        hourly_messages = self.db.get_hourly_messages()
        new_friends_count = 0
        if hourly_messages is not None:
            for key in hourly_messages:
                message = hourly_messages[key]
                if message['is_step'] and message['text'] == 'Accept friend':
                    new_friends_count += 1
        return new_friends_count

    @property
    def hourly_messages_count(self):
        hourly_messages = self.db.get_hourly_messages()
        messages_count = 0
        if hourly_messages is not None:
            for key in hourly_messages:
                message = hourly_messages[key]
                if message['is_incoming'] == False:
                    messages_count += 1
        return messages_count

    # Methods
    def fetch(self):
        bot_data = self.db.get_bot()
        bot_data = bot_data if isinstance(bot_data, dict) else {}
        self.account_username = bot_data['account'] if 'account' in bot_data else None
        self.account2_username = bot_data['account2'] if 'account2' in bot_data else None
        self.__pause = bot_data['pause'] if 'pause' in bot_data else False
        self.__status = bot_data['status'] if 'status' in bot_data else None
        self.__message = bot_data['message'] if 'message' in bot_data else None
        self.last_message_timestamp = bot_data['last_message_timestamp'] if 'last_message_timestamp' in bot_data else 0
        self.__last_screenshot_url = bot_data['last_screenshot_url'] if 'last_screenshot_url' in bot_data else None
        self.__last_story_timestamp = bot_data['last_story_timestamp'] if 'last_story_timestamp' in bot_data else None
        self.__last_traffic_counting_timestamp = bot_data['last_traffic_counting_timestamp'] if 'last_traffic_counting_timestamp' in bot_data else None
        self.__traffic = bot_data['traffic'] if 'traffic' in bot_data else None
        self.__router_volume = bot_data['router_volume'] if 'router_volume' in bot_data else 0
        self.__router_mode = bot_data['router_mode'] if 'router_mode' in bot_data else False
        self.__last_router_mode_run_timestamp = bot_data['last_router_mode_run_timestamp'] if 'last_router_mode_run_timestamp' in bot_data else None

    def to_dict(self):
        return {
            'config': self.config.to_dict(),
            'status': self.status,
            'message': self.message,
            'last_message_timestamp': self.last_message_timestamp,
            'last_screenshot_url': self.last_screenshot_url,
            'last_story_timestamp': self.last_story_timestamp,
            'last_traffic_counting_timestamp': self.last_traffic_counting_timestamp,
            'traffic': self.traffic
        }

    def capture_screen(self, monitor=None):
        screenshot = self.eyes.capture_screen_png(monitor=monitor)
        self.last_screenshot_url = self.db.upload_screenshot(screenshot)

    def log(self, data):
        print(data)

    def log_new_friend(self):
        new_friend_message = chat.Message(chat=chat.Chat(self), message_id=0, is_incoming=False, is_step=True, text='Accept friend', is_photo=False)
        new_friend_message.log()
