from src.models.classes.config import LocalConfig


class Account:
    def __init__(self, bot):
        self.bot = bot
        self.fetch(bot.account_username)

    # Properties
    @property
    def chats_count(self):
        return self.__chats_count

    @chats_count.setter
    def chats_count(self, chats_count):
        self.__chats_count = chats_count
        self.bot.db.update_account(self)

    # Methods
    def fetch(self, account_username):
        account_data = self.bot.db.get_account(account_username)
        self.username = account_data['username'] if account_data and 'username' in account_data else account_username
        self.step1_video_url = account_data['step1_video_url'] if account_data and 'step1_video_url' in account_data else ''
        self.__chats_count = account_data['chats_count'] if account_data and 'chats_count' in account_data else 0

    def to_dict(self):
        return {
            'chats_count': self.chats_count
        }

    def change_account(self):
        self.fetch(self.bot.account2_username if self.username == self.bot.account_username else self.bot.account_username)
