from src.models.classes.config import LocalConfig
import firebase_admin
from firebase_admin import credentials, db
from google.cloud import storage
from google.oauth2 import service_account
from datetime import datetime


class Db:
    def __init__(self):
        config = LocalConfig()
        self.bot_id = config.bot_id
        self.db = db

        try:
            cred = credentials.Certificate("./key.json")
            firebase_admin.initialize_app(cred, {'databaseURL': 'https://' + config.app_name + '.firebaseio.com/'})

        except ValueError:
            pass

    # Config
    def get_config(self):
        return db.reference('config').get()

    def get_bot_config(self):
        return db.reference('bots/'+self.bot_id+'/config').get()

    # Account
    def get_account(self, account_username):
        return db.reference('accounts/'+account_username).get()

    def update_account(self, account):
        if account.username:
            db.reference('accounts/'+account.username).update(account.to_dict())

    # Bot
    def create_bot(self) -> str:
        last_bot_key = list(db.reference('bots').order_by_key().limit_to_last(1).get().keys())[0]
        new_bot_key = str(int(last_bot_key) + 1)
        db.reference('bots').child(new_bot_key).set({'status': 'created'})
        self.bot_id = new_bot_key
        return new_bot_key

    def get_bot(self):
        return db.reference('bots/'+self.bot_id).get()

    def update_bot(self, bot):
        if self.bot_id:
            db.reference('bots/'+self.bot_id).update(bot.to_dict())

    def set_bot_key_value(self, key, value):
        if self.bot_id and len(key) > 0:
            db.reference('bots/'+self.bot_id+'/'+key).set(value)

    # Chats
    def get_chats_count(self, account_username) -> int:
        chats = db.reference('chats/'+account_username).get(shallow=True)
        return len(list(chats)) if chats else 0

    def get_chat(self, account_username, chat_id):
        chat = db.reference('chats/'+account_username+'/'+chat_id).get()
        return chat if chat else {}

    def update_chat(self, account_username, chat):
        if chat.id:
            db.reference('chats/'+account_username+'/'+chat.id).update(chat.to_dict())

    def log_message(self, message):
        if message.id and self.bot_id:
            db.reference('messages/bot'+self.bot_id+'/'+message.timestamp[:13]+'/'+message.id).update(message.to_dict())

    def get_hourly_messages(self):
        return db.reference('messages/bot'+self.bot_id+'/'+datetime.utcnow().isoformat()[:13]).get()

    # Storage
    def upload_screenshot(self, file, prefix=None) -> str:
        # name = str(datetime.datetime.today().timestamp())+'.png'
        name = 'bot'+self.bot_id+'.png'
        name = prefix+'_'+name if prefix else name

        cred = service_account.Credentials.from_service_account_file('./key.json')
        app_name = LocalConfig().app_name
        storage_client = storage.Client(project=app_name, credentials=cred)
        bucket = storage_client.get_bucket(app_name+'.appspot.com')
        blob = bucket.blob(name)
        blob.upload_from_string(file, content_type='image/png')

        return blob.public_url
