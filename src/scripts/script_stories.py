from src.scripts.script_base import Script
from datetime import datetime, timedelta
import random


class ScriptStories(Script):
    def start(self):
        self.bot.status = 'Adding stories'
        self.open_stories_screen()
        self.add_stories()

    def open_stories_screen(self):
        self.bot.hands.open_camera_screen()
        self.bot.hands.scroll('down')
        for _ in range(3):
            self.bot.hands.swipe('left')
        self.bot.hands.swipe('right')

    def add_stories(self):
        stories_post_num = int(datetime.utcnow().hour / 4)
        for i in range(1):
            self.bot.hands.long_press(coords=[
                self.bot.window.left + 100 + (200 * int(stories_post_num/2)),
                self.bot.window.top + 50 + 200 * (1 + (stories_post_num % 2)) + 200 * random.randint(0, 1) + 200 * (stories_post_num % 2)
            ])
        self.bot.hands.click(coords=[self.bot.window.right - 20, self.bot.window.bottom - 20], sleep=3)
        if self.bot.hands.click('my_story.png', 3):
            self.bot.hands.click(coords=[self.bot.window.right - 20, self.bot.window.bottom - 20], sleep=3)
            self.bot.last_story_timestamp = datetime.utcnow().isoformat()
            self.bot.hands.back(2)
