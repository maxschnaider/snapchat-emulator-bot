from src.scripts.script_base import Script
from src.models.classes.account import Account
import time


class ScriptAccounts(Script):
    def start(self):
        self.bot.status = 'Changing account'
        if self.logout():
            print('Logged out!')
            self.bot.account.change_account()

    def logout(self):
        if self.bot.hands.open_profile_screen():
            for _ in range(50):
                time.sleep(0.2)
                if self.bot.hands.click('profile_settings_button.png'):
                    for _ in range(7):
                        self.bot.hands.scroll('down')
                    self.bot.hands.click('logout_button.png')
                    self.bot.hands.click('logout_no_save_button.png')
                    return self.bot.hands.click('logout_accept_button.png', sleep=10)

    def restart_emulator(self):
        return
