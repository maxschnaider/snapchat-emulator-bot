from src.scripts.script_base import Script
from src.scripts.script_friends import ScriptFriends
from src.scripts.script_stories import ScriptStories
from src.scripts.script_accounts import ScriptAccounts
from datetime import datetime, timedelta
import time
import re


class ScriptChats(Script):
    def __init__(self, bot):
        super().__init__(bot)
        self.find_chat_fail_try_count = 0

    def start(self):
        while True:
            self.bot.fetch()

            # self.check_router_mode()
            # self.count_traffic_if_time()
            # self.add_stories_if_time()

            # self.change_account_if_time()

            if self.bot.config.messages_hourly_limit > self.bot.hourly_messages_count:
                chat_found = self.find_chat_and_reply()
                if not chat_found:
                    if self.find_chat_fail_try_count > 5:
                        self.add_friends_on_empty_chats()
                        continue
                    self.bot.hands.scroll('down')
            else:
                ScriptFriends(self.bot).start()

            # chat_found = self.find_chat_and_reply()
            # if not chat_found:
            #     if self.find_chat_fail_try_count > 5:
            #         self.add_friends_on_empty_chats()
            #         continue
            #     self.bot.hands.scroll('down')


    # chat screen logic
    def find_chat_and_reply(self):
        self.bot.log('find_chat_and_reply')
        self.bot.status = 'Working on chats'

        self.bot.hands.open_chats_screen()

        chat_coords = self.bot.eyes.find('say_hi.png')
        if chat_coords:
            self.reply_chat(chat_coords=chat_coords, say_hi=True)
        else:
            chat_coords = self.bot.eyes.find('new_message.png')
            if chat_coords:
                self.reply_chat(chat_coords)
            # elif not self.bot.hands.click('failed.png', 6):
            else:
                self.find_chat_fail_try_count += 1
                return

        self.find_chat_fail_try_count = 0
        return True

    def reply_chat(self, chat_coords, say_hi=False):
        self.bot.log('reply_chat')
        self.bot.status = 'Replying a chat'

        if self.bot.chat.open_chat(chat_coords, say_hi):
            incoming_message_exists = False
            if self.bot.chat.step > 1:
                time.sleep(3)
                incoming_message_exists = self.bot.chat.read_incoming_message()

            if say_hi or (not say_hi and incoming_message_exists) or self.bot.chat.step <= 1:
                self.bot.chat.reply()

    # middleware functions
    def check_router_mode(self):
        if self.bot.router_mode and self.bot.router_volume > 0:
            ScriptFriends(self.bot).add_friends_for_routing()

    def add_friends_on_empty_chats(self):
        self.bot.hands.refresh_chats()
        if not self.find_chat_and_reply():
            ScriptFriends(self.bot).start()

    def add_stories_if_time(self):
        if self.bot.last_story_timestamp is None:
            ScriptStories(self.bot).start()
        else:
            last_story_time = datetime.fromisoformat(self.bot.last_story_timestamp)
            time_since_last_story_posted = datetime.utcnow() + timedelta(seconds=120) - last_story_time
            if time_since_last_story_posted.seconds > 4*60*60 or time_since_last_story_posted.days > 0:
                print(self.bot.last_story_timestamp)
                print(datetime.utcnow().isoformat())
                print(time_since_last_story_posted)
                ScriptStories(self.bot).start()

    def count_traffic_if_time(self):
        if self.bot.last_traffic_counting_timestamp is None:
            ScriptFriends(self.bot).count_traffic()
        else:
            last_traffic_counting_time = datetime.fromisoformat(self.bot.last_traffic_counting_timestamp)
            time_since_last_traffic_counted = datetime.utcnow() + timedelta(seconds=120) - last_traffic_counting_time
            if time_since_last_traffic_counted.seconds > 20*60 or time_since_last_traffic_counted.days > 0:
                ScriptFriends(self.bot).count_traffic()

    def change_account_if_time(self):
        # while datetime.utcnow().hour >= 2 and datetime.utcnow().hour <= 8:
        while datetime.utcnow().hour % 2 == 0:
            self.bot.hands.hide_snapchat()
            time.sleep(60)
        # if (datetime.utcnow().hour % 2 == 0 and self.bot.account.username != self.bot.account_username) or\
        #     (datetime.utcnow().hour % 2 == 1 and self.bot.account.username != self.bot.account2_username):
        # ScriptAccounts(self.bot).start()
