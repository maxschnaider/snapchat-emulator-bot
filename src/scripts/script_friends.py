from src.scripts.script_base import Script
import time
import os
from datetime import datetime
import random


class ScriptFriends(Script):
    def start(self):
        if self.bot.config.friends_hourly_limit <= self.bot.hourly_new_friends_count:
            return

        self.bot.status = 'Adding friends'
        if self.open_friends_screen():
            for _ in range(random.randint(1, 3)):
                self.bot.hands.click('more.png')
                if self.bot.config.friends_hourly_limit > self.bot.hourly_new_friends_count:
                    if self.add_friend():
                        self.say_hi()
                        self.refresh_friends_screen()
                else:
                    return

    def open_friends_screen(self):
        if self.bot.hands.open_profile_screen():
            if not self.bot.eyes.find('friends_plus.png'):
                self.bot.hands.scroll('down')
            if self.bot.hands.click('friends_plus.png', 5):
                self.bot.hands.click('more.png')
                return True

    def count_traffic(self):
        self.bot.status = 'Counting traffic'
        more_btn_click_count = 0
        if self.open_friends_screen():
            self.bot.eyes.read_text()
            while True:
                if self.bot.hands.click('more.png', 2):
                    more_btn_click_count += 1
                    if more_btn_click_count >= 14:
                        self.bot.traffic = 99
                        break
                else:
                    for _ in range(2):
                        if not (self.bot.eyes.find('quick_add.png') or self.bot.eyes.find('more.png')):
                            self.bot.hands.scroll('down')
                    if not self.bot.eyes.find('more.png'):
                        self.bot.traffic = more_btn_click_count * 7
                        break

            self.bot.last_traffic_counting_timestamp = datetime.utcnow().isoformat()

    def add_friends_for_routing(self):
        self.bot.status = 'Auto-add friends'
        if self.open_friends_screen():
            while self.bot.router_volume > 0:
                if self.add_friend():
                    self.bot.router_volume -= 1
                    time.sleep(random.randint(5, 15))
                else:
                    if self.bot.hands.click('more.png'):
                        continue
                    else:
                        if self.bot.eyes.find('quick_add.png'):
                            self.bot.tg.send_message('Боту '+self.bot.id+' не хватило трафа для автодобавления =(')
                            break
                        else:
                            for _ in range(2):
                                if not (self.bot.eyes.find('quick_add.png') or self.bot.eyes.find('more.png')):
                                    self.bot.hands.scroll('down')

                            if not (self.bot.eyes.find('quick_add.png') or self.bot.eyes.find('more.png')):
                                self.open_friends_screen()

            self.bot.tg.send_message('🟢🟢🟢 Бот '+self.bot.id+' закончил автодобавление!')
            self.bot.last_router_mode_run_timestamp = datetime.utcnow().isoformat()
            self.bot.router_volume = 0
            os.abort()

    def add_friend(self):
        def wait_accept_loading() -> bool:
            wait_seconds = 0
            while not self.bot.eyes.find('say_hi_friends.png'):
                time.sleep(1)
                wait_seconds += 1
                if wait_seconds >= 30:
                    self.bot.tg.send_message('🔴🔴🔴 Бот '+self.bot.id+' не может добавить друга =(')
                    return
            return True

        accept_btn_coords = self.bot.eyes.find('accept_friend_btn.png')
        accept_btn_coords = accept_btn_coords if accept_btn_coords else self.bot.eyes.find('accept_friend_btn2.png')
        if accept_btn_coords:
            quick_add_coords = self.bot.eyes.find('quick_add.png')
            if quick_add_coords and quick_add_coords[1] < accept_btn_coords[1]:
                return
            if self.bot.hands.click(coords=accept_btn_coords) and wait_accept_loading():
                self.bot.log_new_friend()
                return True

    def say_hi(self):
        reply_succ = False
        for _ in range(2):
            if reply_succ:
                return

            chat_coords = self.bot.eyes.find('say_hi_friends.png')
            if chat_coords:
                if self.bot.chat.open_chat(chat_coords, say_hi=True):
                    reply_succ = self.bot.chat.reply()
                if not self.back_to_friends():
                    break

    def back_to_friends(self):
        go_back_count = 0
        while not self.bot.eyes.find('say_hi_friends.png'):
            if self.bot.eyes.find('friends_plus.png'):
                return
            self.bot.hands.back()
            go_back_count += 1
            if go_back_count > 3:
                return
        return True

    def refresh_friends_screen(self):
        go_back_count = 0
        while not self.bot.eyes.find('friends_plus.png'):
            self.bot.hands.back()
            go_back_count += 1
            if go_back_count > 5:
                return
        return self.open_friends_screen()
